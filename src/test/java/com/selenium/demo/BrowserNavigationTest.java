package com.selenium.demo;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;

public class BrowserNavigationTest {

    static ExtentReports reports;
    static ExtentTest testLogger;

    @BeforeAll
    public static void setup() {
        ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir")+"/target/sparkreport.html");
        reports = new ExtentReports();
        reports.attachReporter(spark);
    }

    @Test
    public void browserNavTest() {
        testLogger = reports.createTest("Browser Launch Test");
        System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");
        driver.quit();
        testLogger.log(Status.PASS, "Test Passed!");
    }

    @Test
    public void browserSearchTest() throws InterruptedException, IOException {
        testLogger = reports.createTest("Browser Navigatin Test");
        System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");
        driver.findElement(By.name("q")).sendKeys("Hello");
        driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File DestFile=new File(System.getProperty("user.dir")+"/target/screenprint.png");
        FileUtils.copyFile(screenshotFile, DestFile);
        testLogger.addScreenCaptureFromPath(DestFile.getAbsolutePath());
        Thread.sleep(3000);
        driver.quit();
        testLogger.log(Status.FAIL, "Test Passed!");
    }

    @AfterAll
    public static void tearDown() {
        reports.flush();
    }
}
